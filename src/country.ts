export interface country {
    flags: {
        png: string
    },
    name: {
        common: string,
        official: string
    },
    population: number,
    capital: string,
    region: string,
    subregion: string,
    tld: string,
    currencies: {
        PEN: {
            symbol: string
        }
    },
    languages: {},
    borders: [string]
}

export class User{
    constructor(
        public name: string
    ){}
}