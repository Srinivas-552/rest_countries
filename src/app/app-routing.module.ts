import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CountryDetailComponent } from './country-detail/country-detail.component';
import { ListcountriesComponent } from './listcountries/listcountries.component';

const routes: Routes = [
  {path: '', component: ListcountriesComponent},
  {path: 'detail/:name', component: CountryDetailComponent},
  // { path: '', redirectTo: '/', pathMatch: 'full' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
