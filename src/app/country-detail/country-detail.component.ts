import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { CountryService } from '../country.service';
import { country } from 'src/country';

@Component({
  selector: 'app-country-detail',
  templateUrl: './country-detail.component.html',
  styleUrls: ['./country-detail.component.css']
})
export class CountryDetailComponent implements OnInit {

  public countries: country[] = [];

  constructor(
    private route: ActivatedRoute,
    private countryService: CountryService,
    private location: Location
  ) { }

  ngOnInit(): void {
    const name = (this.route.snapshot.paramMap.get('name'))
    this.countryService.getCountries()
      .subscribe(response => {
        this.countries= response.filter(each => each.name.common === name)
      })
  }

}
