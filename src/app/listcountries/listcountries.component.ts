import { Component, OnInit } from '@angular/core';
import { CountryService } from '../country.service';
import { country, User } from 'src/country';

@Component({
  selector: 'app-listcountries',
  templateUrl: './listcountries.component.html',
  styleUrls: ['./listcountries.component.css']
})
export class ListcountriesComponent implements OnInit {

  public countries: country[] = [];

  user = new User('')

  constructor(private countryService: CountryService) { }

  ngOnInit(): void {
    this.countryService.getCountries()
      .subscribe(response => 
        this.countries = response
      )
  }

  onSubmit(countryName: string){
    this.countryService.getCountries()
      .subscribe(response => 
        this.countries = response.filter(eachItem => {
          return eachItem.name.common.toLocaleLowerCase().includes(countryName.toLocaleLowerCase())
        })
        )
  }
}
