import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { country } from 'src/country';
import { Observable, of } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CountryService {

  private fakeApiCountriesUrl:string = "https://restcountries.com/v3.1/all";

  constructor(private http: HttpClient) { }

  getCountries(): Observable<country[]>{
    return this.http.get<country[]>(this.fakeApiCountriesUrl)
  }
}
